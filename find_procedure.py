import os

migrations = 'Migrations'
current_dir = os.path.dirname(os.path.abspath(__file__))
migration_path = os.path.join(current_dir, migrations)


def find_from_file(file_path, search_words):
    try:
        with open(file_path, 'r') as f:
            read_file = f.read()
            if read_file.count(search_words) > 0:
                return True
        return False
    except UnicodeError:
        return False


def main():
    list_files = os.listdir(migration_path)
    print('\n * Absolute path: {}'.format(migration_path))
    try:
        while True:
            new_list_files = []
            search_words = input('\nEntered string: ')
            if len(search_words) != 0:
                number_files = 0
                for file_name in list_files:
                    if '.sql' in file_name:
                        file_path = os.path.join(migration_path, file_name)
                        if find_from_file(file_path, search_words):
                            print(file_name)
                            new_list_files.append(file_name)
                            number_files += 1
                list_files = new_list_files
                print('\n * Number of files: {}'.format(number_files))
    except KeyboardInterrupt:
        print('\nFind procedure is stoped...')


if __name__ == '__main__':
    main()
