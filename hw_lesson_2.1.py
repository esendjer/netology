cook_book_file = 'cook_book.txt'


def get_shop_list_by_dishes(dishes, person_count, cook_book):
    shop_list = {}
    for dish in dishes:
        if cook_book.get(dish) is not None:
            for ingredient in cook_book[dish]:
                new_shop_list_item = dict(ingredient)
                new_shop_list_item['quantity'] *= person_count
                if new_shop_list_item['ingredient_name'] not in shop_list:
                    shop_list[new_shop_list_item['ingredient_name']] = new_shop_list_item
                else:
                    shop_list[new_shop_list_item['ingredient_name']]['quantity'] += new_shop_list_item['quantity']
        else:
            print('Блюдо не найдено в книге.')
    return shop_list


def print_shop_list(shop_list):
    for shop_list_item in shop_list.values():
        print('{} {} {}'.format(shop_list_item['ingredient_name'],
                                shop_list_item['quantity'],
                                shop_list_item['measure']))


def create_shop_list(cook_book):
    person_count = input('Введите нужное количество человек: ')
    if person_count.isdigit():
        person_count = int(person_count)
        dishes = input('Введите блюда в расчете на одного человека (через запятую): ').split(', ')
        if len(dishes) > 0:
            for iter_dish in range(len(dishes)):
                dishes[iter_dish] = dishes[iter_dish].capitalize()
            shop_list = get_shop_list_by_dishes(dishes, person_count, cook_book)
            print_shop_list(shop_list)
            return
    print('Ввод не корректный!')


def get_cook_book():
    cook_book = {}
    element = []
    dish = ''
    count_ingredients = 0
    order = 0
    with open(cook_book_file, encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if len(line.strip()) == 0:
                order = 0
                if len(element) > 0 and dish:
                    cook_book[dish] = element
                    element = []
                continue
            if order == 0:
                dish = line
            if order == 1:
                count_ingredients = int(line)
                order += 1
                continue
            if order == 2:
                (ingredient_name, quantity, measure) = line.split(' | ')
                element.append({'ingredient_name': ingredient_name, 'quantity': int(quantity), 'measure': measure})
                count_ingredients -= 1
            if count_ingredients == 0:
                order += 1
    return cook_book


def main():
    create_shop_list(get_cook_book())
    quit(0)


if __name__ == '__main__':
    main()
