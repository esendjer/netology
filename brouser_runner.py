import sys
import webbrowser

url = 'https://netology.ru/'


def main(arg: str):
    webbrowser.open(arg)


if __name__ == '__main__':
    if len(sys.argv) == 2 and (sys.argv[1].count('http') or sys.argv[1].count('https')):
        print(sys.argv[1])
        main(sys.argv[1])
    else:
        main(url)
