import requests
import os
from datetime import datetime

from ya_translate_conf import *

datetime_format = '%Y_%m_%d__%H_%M_%S'


def read_file(ifile):
    with open(ifile, 'r') as f:
        print('\n * Абсолютный путь до входного файла: {}'.format(ifile))
        txt_from_file = f.read()
    return txt_from_file


def write_file(ofile, text):
    with open(ofile, 'w') as f:
        print(' * Абсолютный путь до файла с переводом: {}'.format(ofile))
        f.write(text)


def translate_it(text, src_lang='en', dst_lang='ru'):
    params_detect = {
        'key': key,
        'text': text
    }
    params_trans = {
        'key': key,
        'format': 'plain',
        'text': text
    }

    req_detect = requests.post(url_detect, params=params_detect)
    if req_detect.status_code == 200:
        src_lang = req_detect.json()['lang']

    params_trans['lang'] = '{}-{}'.format(src_lang, dst_lang)

    req_translate = requests.post(url, params=params_trans)
    if req_translate.status_code == 200:
        result_text = req_translate.json()['text'][0]
        return result_text


def main():
    list_files = []
    list_files_incur_dir = os.listdir(os.curdir)
    for item_file in list_files_incur_dir:
        if item_file.endswith('.txt'):
            list_files.append(item_file)
    for name_ifile in list_files:
        abs_path_to_ifile = '{}/{}'.format(os.path.abspath(os.curdir), name_ifile)
        name_ofile = '{}-translated-{}'.format(datetime.strftime(datetime.now(), datetime_format), name_ifile)
        abs_path_to_ofile = '{}/{}'.format(os.path.abspath(os.curdir), name_ofile)
        read_text = read_file(abs_path_to_ifile)
        translate_txt = translate_it(read_text, dst_lang='ro')
        write_file(abs_path_to_ofile, translate_txt)


if __name__ == '__main__':
    main()

