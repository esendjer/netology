import json
import xml.etree.ElementTree as ET

files_for_read = ['newsafr.json', 'newsafr.xml']


class LongWorldsCalc:
    def __init__(self, list_words: list, len_word=6):
        self.len_word = len_word
        self.list_words = list_words

    def get_list_long_words(self):
        dict_long_words = {}
        for word in self.list_words:
            if len(word) > self.len_word:
                if word not in dict_long_words.keys():
                    dict_long_words[word] = 1
                else:
                    dict_long_words[word] += 1
        result_sort = sorted(dict_long_words.items(), key=lambda x: x[1], reverse=True)
        return result_sort


def print_list_worlds(list_wordls: list):
    for key, value in list_wordls[:10]:
        print(' * Word: {}, {} times'.format(key, value))


def main():
    for file_name in files_for_read:
        print('\nFile name {}:'.format(file_name))
        words = []
        with open(file_name, 'r') as reading_file:
            if file_name.count('.json') != 0:
                json_data = json.load(reading_file)
                for item in json_data['rss']['channel']['items']:
                    words = words + item['description'].split(' ')
                calc_json = LongWorldsCalc(words)
                print_list_worlds(calc_json.get_list_long_words())
            if file_name.count('.xml') != 0:
                parser = ET.XMLParser(encoding='utf-8')
                xml_tree = ET.fromstring(reading_file.read(), parser=parser)
                xml_list = xml_tree.findall('channel/item')
                for item in xml_list:
                    description = item.find('description').text
                    words = words + description.split(' ')
                calc_xml = LongWorldsCalc(words)
                print_list_worlds(calc_xml.get_list_long_words())


if __name__ == '__main__':
    main()