# Homework for the course netology

---
* `hw_lesson_1.5.py` - Homework for the lecture 1.5 "Classes and their use in Python"

---
* `hw_lesson_2.1.py` - Homework for the lecture 2.1 "Opening and reading a file, writing to a file"

---
* `hw_lesson_2.3` - Directory with omework for the lecture 2.5 "Working with different data formats"

---
* `find_procedure.py` - Homework for the lecture "Working with folders, paths, calling other programs". Parts 1 and 2.
* `brouser_runner.py` - Homework for the lecture "Working with folders, paths, calling other programs". Part 3.

---
* `hw_lesson_3.1.py` - Homework for the lecture "Working with requests library, http requests".
* `ya_translate_conf.py` - Additional file for `hw_lesson_3.1.py`.
